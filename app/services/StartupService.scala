package services

import com.google.inject.AbstractModule

class StartupService extends AbstractModule {
  override def configure(): Unit = {
    bind(classOf[InitDB]).asEagerSingleton()
  }
}
