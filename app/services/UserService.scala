package services

import business.genException
import com.google.inject.{Inject, Singleton}
import dao.UserDAO
import models.User
import org.joda.time.DateTime
import play.api.Logger

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.language.postfixOps

@Singleton
class UserService @Inject()(val userDAO: UserDAO, implicit val ec: ExecutionContext) {

  val userList: ArrayBuffer[User] = new ArrayBuffer[User]()

  var lastSyncTime: DateTime = DateTime.now

  def getUser(userName: String): Option[User] = {
    var user = userList.find(_.UserName == userName)
    if (user.isEmpty) {
      Await.ready(refreshAllUsers(), 2 seconds)
      user = userList.find(_.UserName == userName)
    }
    user
  }

  def getUser(userid: Long): Option[User] = {
    var user = userList.find(_.Id == userid)
    if (user.isEmpty) {
      Await.ready(refreshAllUsers(), 2 seconds)
      user = userList.find(_.Id == userid)
    }
    user
  }

  def getUserNameC(username: String): String = {
    getUser(username) match {
      case Some(ue) ⇒ s"${ue.LastName.getOrElse("")} ${ue.FirstName.getOrElse("")}"
      case None ⇒ username
    }
  }

  def getUserNameC(userid: Long): String = {
    getUser(userid) match {
      case Some(ue) => s"${ue.LastName.getOrElse("")} ${ue.FirstName.getOrElse("")}"
      case None => ""
    }
  }

  def cleanCache(): Unit = {
    userList.clear()
  }

  def cleanCacheByGroup(groupid: String): Unit = {
    //    userList.filter(_.Groups.get.exists(_.GroupId == groupid)).clear()
  }

  def listAccountSetByName(name: String): Seq[String] = {
    val gap = DateTime.now.getMillis - lastSyncTime.getMillis
    if (userList.isEmpty) getUser("")
    val loName = name.toLowerCase()
    userList.filter(ue ⇒ s"${ue.LastName.getOrElse("").toLowerCase()} ${ue.FirstName.getOrElse("").toLowerCase()}".indexOf(loName) >= 0).map {
      ue ⇒
        ue.UserName
    }
  }

  private def refreshAllUsers(): Future[Any] = {

    val gap = DateTime.now.getMillis - lastSyncTime.getMillis
    if (gap > 20000 || userList.isEmpty) {
      userDAO.findAll() map {
        users =>
          userList.clear()
          userList ++= users
      } recoverWith {
        case ex: Exception ⇒
          val error = genException(ex, "refreshAllUsers")
          Logger.logger.error(error.getMessage)
          Future.failed(error)
      }
    } else {
      Future.successful((): Unit)
    }
  }
}