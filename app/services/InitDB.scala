package services

import com.google.inject.Inject
import dao.{CodeDAO, EquipmentDAO, EquipmentUseRecordDAO, UserDAO}

import scala.concurrent.ExecutionContext

/**
  * Created by zjw on 2017/12/22. 
  */

class InitDB @Inject()(
                        val userDAO: UserDAO,
                        val codeDAO: CodeDAO,
                        val equipmentDAO: EquipmentDAO,
                        val equipmentUseRecordDAO: EquipmentUseRecordDAO,
                        implicit val ec: ExecutionContext
                      ) {
  userDAO.createTable()
  codeDAO.createTable()
  equipmentDAO.createTable()
  equipmentUseRecordDAO.createTable()
}
