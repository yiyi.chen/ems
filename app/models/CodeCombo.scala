package models

import play.api.libs.json.Json

case class CodeCombo(
                      var Key: String = "",
                      var Desc: String = ""
                    )

object CodeCombo extends ((String, String) => CodeCombo) {
  implicit val codeComboFormat = Json.format[CodeCombo]
}
