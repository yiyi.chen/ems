package models

import util.json._
import org.joda.time.DateTime
import play.api.libs.json.Json

case class AuditInfo(
                      var Last: Option[Double] = None,
                      var Next: Option[Double] = None,
                      var Creator: Option[Long] = None,
                      var CreateTime: Option[DateTime] = None,
                      var Updater: Option[Long] = None,
                      var UpdateTime: Option[DateTime] = None
                    )

object AuditInfo extends ((Option[Double], Option[Double], Option[Long], Option[DateTime], Option[Long], Option[DateTime]) => AuditInfo) {
  implicit val auditInfoFormat = Json.format[AuditInfo]
}