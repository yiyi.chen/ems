package models

import play.api.libs.json.Json

case class ProcStatus(
                       var Status: String = "0",
                       var RowAffected: Int = 0,
                       var Id: Option[Long] = None,
                       var Code: Option[String] = None,
                       var ErrCode: Int = 0,
                       var CheckResult:Option[Boolean]=Some(false),
                       var EMsg: String = ""
                     )

object ProcStatus {
  implicit val procStatusFormat = Json.format[ProcStatus]
}
