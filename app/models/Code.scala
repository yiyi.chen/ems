package models

import util.json._
import org.joda.time.DateTime
import play.api.libs.json.Json

case class Code(
                 var SysCode: String = "",
                 var ModCode: String = "",
                 var Key: String = "",
                 var Description: String = "",
                 var IsHardCoded: Option[Boolean] = None,
                 var Point01: Option[Double] = None,
                 var Point02: Option[Double] = None,
                 var Point03: Option[Double] = None,
                 var Point04: Option[Double] = None,
                 var String01: Option[String] = None,
                 var String02: Option[String] = None,
                 var String03: Option[String] = None,
                 var String04: Option[String] = None,
                 var IsTag: Option[Boolean] = None,
                 var Date01: Option[DateTime] = None,
                 var Date02: Option[DateTime] = None,
                 var Date03: Option[DateTime] = None,
                 var Date04: Option[DateTime] = None,
                 var Audit: Option[AuditInfo] = None
               ) extends AuditAttr

object Code {

  implicit val codeFormat = Json.format[Code]
}
