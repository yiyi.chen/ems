package models

import play.api.libs.json.Json

case class UserPassword(
                         var Id: Long = 0L,
                         var OldPassword: String = "",
                         var NewPassword: String = ""
                       )

object UserPassword {
  implicit val userPasswordFormat = Json.format[UserPassword]
}
