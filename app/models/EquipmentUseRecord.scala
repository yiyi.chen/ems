package models

import org.joda.time.DateTime
import play.api.libs.json.Json
import util.json._

case class EquipmentUseRecord(
                               var Id: Long = 0L,
                               var EquipmentNo: String = "",
                               var CreateTime: Option[DateTime] = None,
                               var UserId: Long = 0L,
                               var Status: Option[String] = None
                             )

object EquipmentUseRecord {
  implicit val equipmentUseRecordFormat = Json.format[EquipmentUseRecord]
}
