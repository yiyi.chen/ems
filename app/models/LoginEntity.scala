package models

import play.api.libs.json.{Json, OFormat}


case class LoginEntity(
                        var UserName: String = "",
                        var Password: String = "",
                        var KeyId: String = ""
                      )

object LoginEntity {
  implicit val loginEntityFormat: OFormat[LoginEntity] = Json.format[LoginEntity]
}
