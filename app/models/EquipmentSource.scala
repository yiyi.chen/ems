package models

import org.joda.time.DateTime
import play.api.libs.json.Json
import util.json._
case class EquipmentSource(
                             var Provider: Option[String] = None,
                             var OwnDept: Option[String] = None,
                             var Keeper: Option[Long] = None,
                             var CheckUnit: Option[String] = None,
                             var SourceWay: Option[String] = None,
                             var CheckDate: Option[DateTime] = None,
                             var CheckDateType: Option[String] = None,
                             var CheckInterval: Option[Int] = None,
                             var CertNo: Option[String] = None,
                             var ProcuPrice: Option[BigDecimal] = None,
                             var ProcuDate: Option[DateTime] = None,
                             var EffectiveDate: Option[DateTime] = None,
                             var EffectiveDateType: Option[String] = None,
                             var NextCheckDate: Option[DateTime] = None,
                             var NextCheckDateType: Option[String] = None,
                             var SourceReq: Option[String] = None,
                             var Memo: Option[String] = None
                           )

object EquipmentSource extends ((Option[String], Option[String], Option[Long], Option[String], Option[String], Option[DateTime], Option[String], Option[Int], Option[String], Option[BigDecimal], Option[DateTime], Option[DateTime], Option[String], Option[DateTime], Option[String], Option[String], Option[String]) => EquipmentSource) {
  implicit val stdInstrueSourceFormat = Json.format[EquipmentSource]
}
