package models


import org.joda.time.DateTime

trait AuditAttr {
  var Audit: Option[AuditInfo]

  def createAudit(uid: Long = 0L): Unit = {
    if (Audit.isEmpty)
      Audit = Some(AuditInfo())

    Audit.get.Creator = Some(uid)
    Audit.get.CreateTime = Some(DateTime.now)
  }

  def updateAudit(uid: Long = 0L): Unit = {
    if (Audit.isEmpty)
      Audit = Some(AuditInfo())

    Audit.get.Updater = Some(uid)
    Audit.get.UpdateTime = Some(DateTime.now)
  }
}