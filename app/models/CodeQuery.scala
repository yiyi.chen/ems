package models

import org.joda.time.DateTime

case class CodeQuery(
                      var code: Code,
                      var p01e: Option[Double] = None,
                      var p02e: Option[Double] = None,
                      var p03e: Option[Double] = None,
                      var p04e: Option[Double] = None,
                      var d01e: Option[DateTime] = None,
                      var d02e: Option[DateTime] = None,
                      var d03e: Option[DateTime] = None,
                      var d04e: Option[DateTime] = None
                    )
