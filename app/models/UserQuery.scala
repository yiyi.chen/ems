package models

import org.joda.time.DateTime
import play.api.libs.json.Json
import util.json._
case class UserQuery(
                      var User: Option[User] = None,
                      var LastVisitE: Option[DateTime] = None
                    )

object UserQuery {
  implicit val userQueryFormat = Json.format[UserQuery]
}