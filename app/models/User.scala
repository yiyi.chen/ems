package models

import util.json._
import org.joda.time.DateTime
import play.api.libs.json.Json

case class User(
                 var Id: Long = 0L,
                 var KeyId: Option[String] = None,
                 var DeptCode: String = "",
                 var UserName: String = "",
                 var Password: String = "",
                 var EMail: Option[String] = None,
                 var Mobile: Option[String] = None,
                 var FirstName: Option[String] = None,
                 var LastName: Option[String] = None,
                 var Name: Option[String] = None,
                 var IsActive: Option[Boolean] = None,
                 var IsSuperuser: Option[Boolean] = None,
                 var LastVisit: Option[DateTime] = None,
                 var Audit: Option[AuditInfo] = None
               ) extends AuditAttr

object User {
  implicit val userFormat = Json.format[User]
}
