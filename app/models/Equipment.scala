package models

import play.api.libs.json.Json
import util.json._
case class Equipment(var Id: Long = 0L,
                     var Code: String = "",
                     var No:String="",
                     var Name: String = "",
                     var Pinyin: Option[String] = None,
                     var ModeNo: Option[String] = None,
                     var ManufNo: Option[String] = None,
                     var CheckRange: Option[String] = None,
                     var Uncertainty: Option[String] = None,
                     var Producer: Option[String] = None,
                     var Usage: Option[String] = None,
                     var FileNo: Option[String] = None,
                     var IsStandard: Option[Boolean] = None,
                     var InstruementType: Option[String] = None,
                     var Source: Option[EquipmentSource] = None,
                     var Status: Option[String] = None,

                    )

object Equipment {
  implicit val equipmentFormat = Json.format[Equipment]
}