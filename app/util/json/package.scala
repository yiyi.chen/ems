package util

import java.sql.{Date, Timestamp}

import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import play.api.libs.json._

package object json {

  implicit val jodaDateTimeFormat = new Format[DateTime] {
    val isoFmt = ISODateTimeFormat.dateTime()
    val csFmt = ISODateTimeFormat.dateHourMinuteSecond()

    def reads(v: JsValue): JsResult[DateTime] = {
      v match {
        case JsNumber(num) =>
          JsSuccess(new DateTime(num.longValue()))
        case JsString(dstr) ⇒
          try {
            JsSuccess(isoFmt.parseDateTime(dstr))
          } catch {
            case _: Throwable ⇒
              try {
                JsSuccess(csFmt.parseDateTime(dstr))
              } catch {
                case e: Throwable =>
                  JsError("Bad Format - Joda DateTime")
              }
          }
        case _ =>
          JsError(s"Bad format-$v")
      }
    }

    def writes(v: DateTime): JsValue = {
      JsString(v.toString(isoFmt))
    }
  }

  implicit val sqlDateFormat = new Format[java.sql.Date] {
    def reads(v: JsValue): JsResult[java.sql.Date] = {
      v match {
        case JsNumber(num) =>
          JsSuccess(new Date(num.longValue()))
        case _ =>
          JsError("Bad format")
      }
    }

    def writes(v: Date): JsValue = {
      JsNumber(v.getTime)
    }
  }

  implicit val sqlTimestampFormat = new Format[Timestamp] {
    def reads(v: JsValue): JsResult[Timestamp] = {
      v match {
        case JsNumber(num) =>
          JsSuccess(new Timestamp(num.longValue()))
        case _ =>
          JsError("Bad format")
      }
    }

    def writes(v: Timestamp): JsValue = {
      JsNumber(v.getTime)
    }
  }
}
