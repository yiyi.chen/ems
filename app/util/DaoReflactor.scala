package util

import java.io.PrintWriter

import play.api.Logger

import scala.reflect.runtime.{universe => ru}
import ru._

object DaoReflactor {

  def genSources(name: String, path: String): Unit = {
    val rm = runtimeMirror(getClass.getClassLoader)
    val cs = rm.staticClass(name)

    val lines = (
      new StringBuilder,
      new StringBuilder,
      new StringBuilder,
      new StringBuilder,
      new StringBuilder,
      new StringBuilder
    )

    reflectInners(cs, lines)

    //    Logger.debug(lines._1.toString())
    //    Logger.debug(lines._2.toString())
    //    Logger.debug(lines._3.toString())
    //    Logger.debug(lines._4.toString())
    //    Logger.debug(lines._5.toString())
    //    Logger.debug(lines._6.toString())

    val cName = cs.name.toString
    val cNamePrefix = cName.head.toLower

    val daoFile =
      s"""
         |import com.google.inject.Inject
         |import models.system.AuditInfo
         |import org.joda.time.DateTime
         |import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
         |import slick.jdbc.JdbcProfile
         |import scala.concurrent.ExecutionContext
         |
         |trait ${cName}Component {
         |  self: HasDatabaseConfigProvider[JdbcProfile] =>
         |
         |  import profile.api._
         |
         |  final class ${cName}Table(tag: Tag) extends Table[$cName](tag, "t0000") {
         |    import dao.CustomColumnTypes._
         |
         |    ${lines._1.toString}
         |
         |    def * = (${lines._2.toString}).shaped <>
         |      ( {
         |        case (${lines._3.toString}) =>
         |          $cName(${lines._4.toString})
         |        }, { $cNamePrefix: $cName =>
         |          Some((${lines._5.toString}))
         |        })
         |  }
         |}
         |
         |class ${cName}DAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
         |                               implicit val ec: ExecutionContext) extends ${cName}Component
         |                               with HasDatabaseConfigProvider[JdbcProfile] {
         |
         |  import profile.api._
         |
         |  private lazy val ${cName.toLowerCase}s = TableQuery[${cName}Table]
         |
         |  def createTable(): Unit = {
         |    db.run(
         |      ${cName.toLowerCase}s.schema.create
         |    )
         |  }
         |}
       """.stripMargin

    val csFile =
      s"""
         |public $cName {
         |  ${lines._6.toString}
         |}
       """.stripMargin

    //    Logger.debug(daoFile)
    //    Logger.debug(csFile)


    var writer = new PrintWriter(s"$path/${cName}DAO.scala")
    writer.write(daoFile)
    writer.close()

    writer = new PrintWriter(s"$path/$cName.cs")
    writer.write(csFile)
    writer.close()
  }

  private def reflectInners(classSymbol: ClassSymbol,
                            lines: (StringBuilder, StringBuilder, StringBuilder, StringBuilder, StringBuilder, StringBuilder),
                            prefix: String = ""): Unit = {
    val rawName = classSymbol.name.toString
    val objectNamePrefix = rawName.head.toLower

    for (tp <- classSymbol.primaryConstructor.asMethod.paramLists.head) {
      val fieldName = tp.name.toString
      val fieldTypeSignature = tp.typeSignature
      val fieldTypeName = fieldTypeSignature.typeSymbol.name.toString

      if (fieldTypeSignature.typeSymbol.asClass.isPrimitive) {
        lines._1.append(s"""def $prefix${fieldName.toLowerCase} = column[$fieldTypeName]("${prefix.toLowerCase}${fieldName.toLowerCase}")""").
          append(System.getProperty("line.separator"))

        if (lines._2.isEmpty) {
          lines._2.append(s"$prefix${fieldName.toLowerCase}")
          lines._3.append(s"$prefix${fieldName.toLowerCase}")
          lines._4.append(s"$prefix${fieldName.toLowerCase}")
          lines._5.append(s"$objectNamePrefix.$fieldName")
        } else {
          lines._2.append(",").append(s"$prefix${fieldName.toLowerCase}")
          lines._3.append(",").append(s"$prefix${fieldName.toLowerCase}")
          lines._4.append(",").append(s"$prefix${fieldName.toLowerCase}")
          lines._5.append(",").append(s"$objectNamePrefix.$fieldName")
        }

        lines._6.append(s"public ${fieldTypeName.toLowerCase} $fieldName {get; set;}").append(System.getProperty("line.separator"))
      } else {
        val (innerTypeName, isOption, isPrimitive) = if (fieldTypeName == "Option") {
          (fieldTypeSignature.typeArgs.head.typeSymbol.name.toString, true,
            fieldTypeSignature.typeArgs.head.typeSymbol.asClass.isPrimitive)
        } else {
          (fieldTypeName, false, false)
        }

        val innerTypeFullName = if (Seq("Array", "Seq").contains(innerTypeName))
          s"$innerTypeName[${fieldTypeSignature.typeArgs.head.typeArgs.head.typeSymbol.name}]"
        else innerTypeName

        val isReflactable = !Seq("String", "DateTime", "BigDecimal", "Array", "Seq").contains(innerTypeName) &&
          !fieldTypeSignature.typeArgs.head.typeSymbol.asClass.isPrimitive

        if (isReflactable) {
          val fullTypeName = fieldTypeSignature.typeArgs.head.typeSymbol.asClass.fullName

          val cs = runtimeMirror(getClass.getClassLoader).staticClass(fullTypeName)
          val lines0 = (
            new StringBuilder,
            new StringBuilder,
            new StringBuilder,
            new StringBuilder,
            new StringBuilder,
            new StringBuilder
          )

          val pfx = innerTypeFullName.filter(_.isUpper).mkString("").toLowerCase

          reflectInners(cs, lines0, pfx)

          lines._1.append(lines0._1)
          if (lines._2.isEmpty) {
            lines._2.append("(").append(lines0._2).append(")")
            lines._3.append(fieldName.toLowerCase)

            if (isOption) {
              lines._4.append(s"Option($innerTypeFullName.tupled.apply(${fieldName.toLowerCase}))")
              lines._5.append(s"$innerTypeFullName.unapply($objectNamePrefix.$fieldName.getOrElse($innerTypeFullName())).get")
            } else {
              lines._4.append(s"$innerTypeFullName.tupled.apply(${fieldName.toLowerCase})")
              lines._5.append(s"$innerTypeFullName.unapply($objectNamePrefix.$fieldName).get")
            }
          } else {
            lines._2.append(",(").append(lines0._2).append(")")
            lines._3.append(",").append(fieldName.toLowerCase)

            if (isOption) {
              lines._4.append(s",Option($innerTypeFullName.tupled.apply(${fieldName.toLowerCase}))")
              lines._5.append(s",$innerTypeFullName.unapply($objectNamePrefix.$fieldName.getOrElse($innerTypeFullName())).get")
            } else {
              lines._4.append(s",$innerTypeFullName.tupled.apply(${fieldName.toLowerCase})")
              lines._5.append(s",$innerTypeFullName.unapply($objectNamePrefix.$fieldName).get")
            }
          }
        } else {
          if (isOption) {
            lines._1.append(s"""def $prefix${fieldName.toLowerCase} = column[Option[$innerTypeFullName]]("${prefix.toLowerCase}${fieldName.toLowerCase}")""").
              append(System.getProperty("line.separator"))
          } else {
            lines._1.append(s"""def $prefix${fieldName.toLowerCase} = column[$innerTypeFullName]("${prefix.toLowerCase}${fieldName.toLowerCase}")""").
              append(System.getProperty("line.separator"))
          }

          if (lines._2.isEmpty) {
            lines._2.append(s"$prefix${fieldName.toLowerCase}")
            lines._3.append(s"$prefix${fieldName.toLowerCase}")
            lines._4.append(s"$prefix${fieldName.toLowerCase}")
            lines._5.append(s"$objectNamePrefix.$fieldName")
          } else {
            lines._2.append(",").append(s"$prefix${fieldName.toLowerCase}")
            lines._3.append(",").append(s"$prefix${fieldName.toLowerCase}")
            lines._4.append(",").append(s"$prefix${fieldName.toLowerCase}")
            lines._5.append(",").append(s"$objectNamePrefix.$fieldName")
          }
        }

        val csTypeName = if (isOption) {
          if (isPrimitive) {
            innerTypeFullName match {
              case "Boolean" =>
                "bool?"
              case _ =>
                s"${innerTypeFullName.toLowerCase}?"
            }
          } else {
            innerTypeFullName match {
              case "DateTime" =>
                "DateTime?"
              case "BigDecimal" =>
                "decimal?"
              case "String" =>
                "string"
              case _ =>
                innerTypeFullName
            }
          }
        } else {
          innerTypeFullName match {
            case "BigDecimal" =>
              "decimal"
            case "String" =>
              "string"
            case _ =>
              innerTypeFullName
          }
        }
        lines._6.append(s"public $csTypeName $fieldName {get; set;}").append(System.getProperty("line.separator"))
      }
    }
  }
}
