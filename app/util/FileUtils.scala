package util

import java.io._
import java.util.zip.{ZipEntry, ZipInputStream, ZipOutputStream}

import org.apache.commons.codec.digest._
import java.nio.file.{Files, Paths}

import play.api.Logger

object FileUtils {
  def zip(out: String, files: Iterable[String]): Unit = {
    val zip = new ZipOutputStream(new FileOutputStream(out))

    files foreach {
      f =>
        val fileName = f.substring(f.lastIndexOf("\\") + 1)
        zip.putNextEntry(new ZipEntry(fileName))
        val in = new BufferedInputStream(new FileInputStream(f))
        var b = in.read()
        while (b > -1) {
          zip.write(b)
          b = in.read()
        }
        in.close()
        zip.closeEntry()
    }
    zip.close()
  }

  def unzip(srcPath: String) {
    val zis = new ZipInputStream(new FileInputStream(srcPath))
    val buf = new Array[Byte](1024)
    var len = 0

    val f = new File(srcPath)
    val outputDir = f.getParent

    var ze = zis.getNextEntry
    while (ze != null) {
      if (ze.isDirectory) {
        val dir = new File(ze.getName)
        dir.mkdir
      }
      else {
        val file = new File(s"$outputDir${File.separator}${ze.getName}")
        val os: BufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file))
        try {
          len = zis.read(buf)
          while (len >= 0) {
            os.write(buf, 0, len)
            len = zis.read(buf)
          }
        }
        finally {
          os.close()
        }
      }

      ze = zis.getNextEntry
    }

    zis.close()
  }

  def getMd5(path: String): String = {
    val bytes = Files.readAllBytes(Paths.get(path))
    val md5 = DigestUtils.md5Hex(bytes)
    Logger.logger.info(md5)
    md5
  }
}
