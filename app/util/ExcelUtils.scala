package util

import java.text.{DecimalFormat, SimpleDateFormat}
import java.util.Date

import org.apache.poi.ss.usermodel.{Cell, CellType, DateUtil, FormulaEvaluator}
import org.joda.time.DateTime

/**
  * Created by Dieter on 15/06/08.
  */
object ExcelUtils {
  def getCellInt(cell: Cell)(implicit eval: FormulaEvaluator): Option[Int] = {
    try {
      if (cell == null) {
        None
      } else {
        cell.getCellType match {
          case CellType.NUMERIC =>
            Some(cell.getNumericCellValue.toInt)
          case CellType.STRING =>
            Some(cell.getRichStringCellValue.toString.toInt)
          case CellType.FORMULA =>
            val cellValue = eval.evaluate(cell)
            cellValue.getCellType match {
              case CellType.NUMERIC =>
                Some(cellValue.getNumberValue.toInt)
              case CellType.STRING =>
                Some(cellValue.getStringValue.toInt)
              case _ =>
                None
            }
          case _ =>
            None
        }
      }
    } catch {
      case ex: Exception =>
        None
    }
  }

  def getCellDouble(cell: Cell)(implicit eval: FormulaEvaluator): Option[Double] = {
    try {
      if (cell == null) {
        None
      } else {
        cell.getCellType match {
          case CellType.NUMERIC =>
            Some(cell.getNumericCellValue)
          case CellType.STRING =>
            Some(cell.getRichStringCellValue.toString.toDouble)
          case CellType.FORMULA =>
            val cellValue = eval.evaluate(cell)
            cellValue.getCellType match {
              case CellType.NUMERIC =>
                Some(cellValue.getNumberValue)
              case CellType.STRING =>
                Some(cellValue.getStringValue.toDouble)
              case _ =>
                None
            }
          case _ =>
            None
        }
      }
    } catch {
      case ex: Exception =>
        None
    }
  }

  def getCellBigDecimal(cell: Cell)(implicit eval: FormulaEvaluator): Option[BigDecimal] = {
    getCellDouble(cell) match {
      case Some(d) => Option(BigDecimal(d))
      case None => None
    }
  }

  def getCellString(cell: Cell)(implicit eval: FormulaEvaluator): Option[String] = {
    try {
      if (cell == null) {
        None
      } else {
        cell.getCellType match {
          case CellType.NUMERIC =>
            val value = cell.getNumericCellValue
            val dt = new DecimalFormat("0.00")
            val longValue = value.toLong
            if (value == longValue) {
              Some(longValue.toString)
            } else {
              Some(value.toString)
            }
          case CellType.STRING =>
            Some(cell.getRichStringCellValue.toString)
          case CellType.FORMULA =>
            val cellValue = eval.evaluate(cell)
            cellValue.getCellType match {
              case CellType.NUMERIC =>
                Some(cellValue.getNumberValue.toString)
              case CellType.STRING =>
                Some(cellValue.getStringValue)
              case _ =>
                None
            }
          case _ =>
            None
        }
      }
    } catch {
      case ex: Exception =>
        None
    }
  }

  def getCellDatetime(cell: Cell)(implicit eval: FormulaEvaluator): Option[DateTime] = {
    getCellDate(cell) match {
      case Some(date) ⇒ Some(new DateTime(date.getTime))
      case None ⇒ None
    }
  }

  def getCellDate(cell: Cell)(implicit eval: FormulaEvaluator): Option[Date] = {
    val sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val sdf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
    val sdf3 = new SimpleDateFormat("yyyy.MM.dd")
    val sdf4 = new SimpleDateFormat("yyyy-MM-dd")
    try {
      if (cell == null) {
        None
      } else {
        cell.getCellType match {
          case CellType.NUMERIC =>
            if (DateUtil.isCellDateFormatted(cell)) {
              Some(cell.getDateCellValue)
            } else {
              None
            }
          case CellType.STRING =>
            try {
              Some(sdf.parse(cell.getRichStringCellValue.getString.trim))
            } catch {
              case ex: Exception =>
                try {
                  Some(sdf2.parse(cell.getRichStringCellValue.getString.trim))
                } catch {
                  case ex: Exception =>
                    try {
                      Some(sdf3.parse(cell.getRichStringCellValue.getString.trim))
                    } catch {
                      case ex: Exception ⇒
                        try {
                          Some(sdf4.parse(cell.getRichStringCellValue.getString.trim))
                        } catch {
                          case ex: Exception ⇒
                            None
                        }
                    }
                }
            }
          case CellType.FORMULA =>
            val cellValue = eval.evaluate(cell)
            cellValue.getCellType match {
              case CellType.NUMERIC =>
                if (DateUtil.isCellDateFormatted(cell)) {
                  Some(cell.getDateCellValue)
                } else {
                  None
                }
              case _ =>
                None
            }
          case _ =>
            None
        }
      }
    } catch {
      case ex: Exception =>
        None
    }
  }

  def getCellBool(cell: Cell)(implicit eval: FormulaEvaluator): Boolean = {
    if (cell == null) {
      false
    } else {
      cell.getCellType match {
        case CellType.BOOLEAN ⇒
          cell.getBooleanCellValue
        case CellType.STRING ⇒
          try {
            cell.getRichStringCellValue.toString.toBoolean
          } catch {
            case ex: Exception ⇒
              false
          }
        case CellType.FORMULA ⇒
          val cellValue = eval.evaluate(cell)
          cellValue.getCellType match {
            case CellType.BOOLEAN ⇒
              cell.getBooleanCellValue
            case CellType.STRING ⇒
              try {
                cell.getRichStringCellValue.toString.toBoolean
              } catch {
                case ex: Exception ⇒
                  false
              }
            case _ ⇒ false
          }
        case _ => false
      }
    }
  }
}
