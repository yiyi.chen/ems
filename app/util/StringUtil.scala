package util

import java.security.MessageDigest

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.collection.mutable.ArrayBuffer

object StringUtil {
  private val HEX_DIGITS = Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f')

  def parseToMD5(raw: String): String = {
    val md5 = MessageDigest.getInstance("MD5")
    val bytes = md5.digest(raw.getBytes())
    val stringBuffer = new StringBuffer()
    for (b <- bytes) {
      val bt = b & 0xff
      if (bt < 16) {
        stringBuffer.append(0)
      }
      stringBuffer.append(Integer.toHexString(bt))
    }

    stringBuffer.toString.toUpperCase()
  }

  def parseToSHA1(raw: String): String = {
    val messageDigest = MessageDigest.getInstance("SHA1")
    messageDigest.update(raw.getBytes)
    val bytes = messageDigest.digest()
    val len = bytes.length
    val buf = new StringBuilder(len * 2)
    var j = 0
    while ( {
      j < len
    }) {
      buf.append(HEX_DIGITS((bytes(j) >> 4) & 0x0f))
      buf.append(HEX_DIGITS(bytes(j) & 0x0f))

      {
        j += 1
        j - 1
      }
    }
    buf.toString()
  }

  def addOne(raw: String): String = {
    val lastnumstr = raw.split("[^1-9]").last
    lastnumstr match {
      case "" =>
        raw
      case _ =>
        val addone = s"${lastnumstr.toLong + 1}"
        s"${raw.substring(0, raw.length - addone.length)}$addone"
    }
  }

  def getAddOnes(raw: String, num: Int): Seq[String] = {
    val ones = ArrayBuffer[String]()
    var chars = raw.toCharArray
    val length = chars.length
    var offset = 0
    var firstno = raw.substring(1).toLong
    for (i <- 1 to num) {
      var cs = chars
      var no = firstno + offset
      var i = 1
      for (c <- no.toString.toCharArray.reverse) {
        cs(length - i) = c
        i = i + 1
      }
      val certno = cs.mkString("")
      ones += certno
      offset = offset + 1
    }
    ones
  }

  def genRandString(bit: Int): String = {
    val r = s"${(Math.random() * (Math.pow(10, bit) - 1)).toString}"
    val rstring = r.substring(0, r.indexOf("."))
    s"${"0" * (bit - rstring.length)}$rstring"
  }

  def subStringByLastLetter(input: String): String = {
    val index = input.toCharArray.lastIndexWhere(c => c.isLetter)
    input.substring(index)
  }

  /**
    * 字符串增强类，隐式扩展为String类型增加数据类型转换扩展方法，返回Option类型对象。
    *
    * 适用于字符串，到以下类型的转换：
    *
    * Int，Long，Boolean，BigDecimal，Joda DateTime，java.sql.Timestamp，java.sql.Date
    *
    * 除toIntOptZero，在转换失败时返回Some(0),其他方法均返回None
    *
    * 调用方法： "xxxxx".扩展方法，如"999.99".toDecimalOpt，返回：Some(BigDecimal(999.99)); "99x.99".toBigDecimalOpt，返回None
    *
    * @param s 扩展的字符串实例对象
    */
  implicit class StringEnhancements(val s: String) {

    import scala.util.control.Exception._

    private val localDateTimeFormatter = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss")

    def toIntOpt = catching(classOf[NumberFormatException]) opt s.toInt

    def toLongOpt = catching(classOf[NumberFormatException]) opt s.toLong

    def toDoubleOpt = catching(classOf[NumberFormatException]) opt s.toDouble

    def toIntOptZero = {
      val i = toIntOpt
      if (i.isDefined) {
        i
      } else {
        Some(0)
      }
    }

    def toBooleanOpt = catching(classOf[IllegalArgumentException]) opt s.toBoolean

    def toBigDecimalOpt = catching(classOf[NumberFormatException]) opt new BigDecimal(new java.math.BigDecimal(s))

    def toJodaDateTimeOpt = {
      val l = toLongOpt
      if (l.isDefined && l.get > 0) {
        Some(new DateTime(l.get))
      } else {
        try {
          Some(DateTime.parse(s, localDateTimeFormatter))
        } catch {
          case _: Any =>
            None
        }
      }
    }

    def toTimestampOpt = {
      val l = toLongOpt
      if (l.isDefined && l.get > 0) {
        Some(new java.sql.Timestamp(l.get))
      } else {
        None
      }
    }

    def toSqlDateOpt = {
      val l = toLongOpt
      if (l.isDefined) {
        Some(new java.sql.Date(l.get))
      } else {
        None
      }
    }

    def toNone = {
      if (s.nonEmpty)
        Some(s)
      else
        None
    }

    //    def toOptBSOBObjectId = {
    //      if (s.nonEmpty) {
    //        if (BSONObjectID.parse(s).getOrElse(BSONObjectID.generate()).stringify == s) {
    //          Option(BSONObjectID.parse(s).get)
    //        } else {
    //          None
    //        }
    //      } else {
    //        None
    //      }
    //    }
  }

  implicit class OptionStringEnhancements(val s: Option[String]) {
    def toRealNone = {
      s match {
        case Some(str) ⇒
          if (str.nonEmpty) {
            s
          } else {
            None
          }
        case None ⇒
          None
      }
    }

    def toOptDouble = {
      s match {
        case Some(str) ⇒
          if (str.nonEmpty) {
            str.toDoubleOpt
          } else {
            None
          }
        case None ⇒
          None
      }
    }

    def toOptInt = {
      s match {
        case Some(str) ⇒
          if (str.nonEmpty)
            str.toIntOpt
          else {
            None
          }
        case None ⇒
          None
      }
    }

    def toOptBigDecimal = {
      s match {
        case Some(str) ⇒
          if (str.nonEmpty) {
            str.toDoubleOpt match {
              case Some(dbl) ⇒ Option(BigDecimal(dbl))
              case None ⇒ None
            }
          } else {
            None
          }
        case None ⇒
          None
      }
    }
  }

}


