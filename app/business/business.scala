
import java.security.MessageDigest
import java.time.format.{DateTimeFormatter, DateTimeParseException}
import java.time.{Instant, LocalDateTime, ZoneId}
import java.util.UUID

import net.sourceforge.pinyin4j.PinyinHelper
import org.apache.commons.codec.digest.DigestUtils
import org.joda.time.DateTime
import play.api.libs.json.Json

package object business {
  def genExceptionMsg(ex: Exception, funcName: String, errorModule: String = "Acorne Error"): String = {
    "%s[%s]:%s".format(errorModule, funcName, ex.getMessage)
  }

  def genException(ex: Exception, funcName: String, errorModule: String = "Acorne Error"): Exception = {
    new Exception(genExceptionMsg(ex, funcName, errorModule), ex)
  }

  def getPinyinFirstCapital(s: String): String = {
    val pinyin = new StringBuilder()

    for (s0 <- s) {
      val pinyins = PinyinHelper.toHanyuPinyinStringArray(s0)
      if (pinyins != null && pinyins.nonEmpty) {
        pinyin.append(pinyins(0)(0).toUpper)
      }
    }

    pinyin.toString()
  }

  def getUUID: String = {
    java.util.UUID.randomUUID().toString
  }

  def parseToMD5(raw: String): String = {
    val md5 = MessageDigest.getInstance("MD5")
    val bytes = md5.digest(raw.getBytes())
    val stringBuffer = new StringBuffer()
    for (b <- bytes) {
      val bt = b & 0xff
      if (bt < 16) {
        stringBuffer.append(0)
      }
      stringBuffer.append(Integer.toHexString(bt))
    }

    stringBuffer.toString.toUpperCase()
  }

  case class Paging(limit: Int, pageCount: Int, skipCount: Int)

  case class MetaInfo(PageCount: Int, Total: Long = 0)

  implicit val metaFormat = Json.format[MetaInfo]

  implicit class StringEnhancements(val s: String) {

    import scala.util.control.Exception._

    private val localDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")

    def toIntOpt = catching(classOf[NumberFormatException]) opt s.toInt

    def toLongOpt = catching(classOf[NumberFormatException]) opt s.toLong

    def toDoubleOpt = catching(classOf[NumberFormatException]) opt s.toDouble

    def toIntOptZero = {
      val i = toIntOpt
      if (i.isDefined) {
        i
      } else {
        Some(0)
      }
    }

    def toBooleanOpt = catching(classOf[IllegalArgumentException]) opt s.toBoolean

    def toBigDecimalOpt = catching(classOf[NumberFormatException]) opt new BigDecimal(new java.math.BigDecimal(s))

    def toJodaDateTimeOpt = {
      val l = toLongOpt
      if (l.isDefined && l.get > 0) {
        Some(new DateTime(l.get))
      } else {
        None
      }
    }

    def toLocalDateTimeOpt = {
      val l = toLongOpt
      if (l.isDefined && l.get > 0) {
        Some(Instant.ofEpochMilli(l.get).atZone(ZoneId.systemDefault()).toLocalDateTime)
      } else {
        try {
          Some(LocalDateTime.parse(s, localDateTimeFormatter))
        } catch {
          case _: DateTimeParseException =>
            None
        }
      }
    }

    def toTimestampOpt = {
      val l = toLongOpt
      if (l.isDefined && l.get > 0) {
        Some(new java.sql.Timestamp(l.get))
      } else {
        None
      }
    }

    def toSqlDateOpt = {
      val l = toLongOpt
      if (l.isDefined) {
        Some(new java.sql.Date(l.get))
      } else {
        None
      }
    }

    def toNone = {
      if (s.nonEmpty)
        Some(s)
      else
        None
    }

    //    def toOptBSOBObjectId = {
    //      if (s.nonEmpty) {
    //        if (BSONObjectID.parse(s).getOrElse(BSONObjectID.generate()).stringify == s) {
    //          Option(BSONObjectID.parse(s).get)
    //        } else {
    //          None
    //        }
    //      } else {
    //        None
    //      }
    //    }
  }

  implicit class OptionStringEnhancements(val s: Option[String]) {
    def toRealNone = {
      s match {
        case Some(str) ⇒
          if (str.nonEmpty) {
            s
          } else {
            None
          }
        case None ⇒
          None
      }
    }

    def toOptDouble = {
      s match {
        case Some(str) ⇒
          if (str.nonEmpty) {
            str.toDoubleOpt
          } else {
            None
          }
        case None ⇒
          None
      }
    }

    def toOptInt = {
      s match {
        case Some(str) ⇒
          if (str.nonEmpty)
            str.toIntOpt
          else {
            None
          }
        case None ⇒
          None
      }
    }

    def toOptBigDecimal = {
      s match {
        case Some(str) ⇒
          if (str.nonEmpty) {
            str.toDoubleOpt match {
              case Some(dbl) ⇒ Option(BigDecimal(dbl))
              case None ⇒ None
            }
          } else {
            None
          }
        case None ⇒
          None
      }
    }
  }

  def genUUIDString32(): String = {
    val u = UUID.randomUUID()
    u.toString.trim.replace("-", "")
  }

  def encryptWithMD5(raw: String): String = {
    val md5 = MessageDigest.getInstance("MD5")
    val bytes = md5.digest(raw.getBytes())
    val stringBuffer = new StringBuffer()
    for (b <- bytes) {
      val bt = b & 0xff
      if (bt < 16) {
        stringBuffer.append(0)
      }
      stringBuffer.append(Integer.toHexString(bt))
    }

    stringBuffer.toString.toUpperCase()
  }

  def encryptWithSHA1Sorted(strings: Seq[String]): String = {
    DigestUtils.sha1Hex(strings.sorted.mkString(""))
  }

  def encrypWithSHA1Sorted(strings: Map[String, Option[String]]): String = {
    val signature = strings.toList.filter(_._2.isDefined).sortBy(_._1) map {
      case (key, Some(value)) ⇒ s"$key=$value"
      case (key, value) ⇒ s"$key=$value"
    } mkString "&"

    DigestUtils.sha1Hex(signature)
  }
}
