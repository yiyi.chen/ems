package business

import com.google.inject.Inject
import dao.UserDAO
import models.{User, UserQuery}

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{ExecutionContext, Future}

class UserBiz @Inject()(
                         val userDAO: UserDAO,
                         implicit val ec: ExecutionContext
                       ) {
  def createUser(name: String, pinyinName: String, email: String, uid: Long): Future[Long] = {
    val u = User(
      UserName = pinyinName.toLowerCase,
      Password = parseToMD5("111111"),
      Name = Some(name),
      EMail = Some(email),
      IsActive = Some(true)
    )
    userDAO.add(u, uid)
  }

  def importUser(users: Seq[User], uid: Long): Future[Int] = {
    val newusers = ArrayBuffer[User]()
    val oldusers = ArrayBuffer[User]()
    userDAO.findAll() flatMap {
      allusers =>
        users foreach {
          user =>
            val u = allusers.find(u => u.UserName == user.UserName)
            if (u.nonEmpty) {
              user.Id = u.getOrElse(User()).Id
              oldusers += user
            } else {
              newusers += user
            }
        }
        userDAO.updateInbatch(oldusers, uid: Long) flatMap {
          _ =>
            userDAO.addInBatch(newusers)
        }
    }
  }

  def listUsers(uid: Long, query: UserQuery, page: Option[Int], limit: Option[Int]): Future[(Int, Int, Seq[User])] = {
    userDAO.findByQuery(query, page, limit) map {
      result =>
        result
    }
  }
}
