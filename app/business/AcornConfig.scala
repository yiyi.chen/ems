package business

import com.google.inject.Inject
import play.api.Configuration

class  AcornConfig @Inject()(
                              config: Configuration
                            ) {
  val recPerPage: Int = config.get[Option[Int]]("acorn.recperpage").getOrElse(20)

  def paging(length: Long, page: Option[Int] = None, per: Option[Int]): Paging = {
    val countPerPage = per match {
      case Some(p) ⇒ if (p > 0) p else recPerPage
      case None ⇒ recPerPage
    }
    val pageCount = Math.ceil(length * 1.0 / countPerPage).toInt
    val skipCount = page match {
      case Some(p) ⇒ if (p >= pageCount) 0 else p * countPerPage
      case None ⇒ 0
    }

    if (skipCount < 0) {
      Paging(0, 0, 0)
    } else {
      Paging(countPerPage, pageCount, skipCount)
    }
  }
}