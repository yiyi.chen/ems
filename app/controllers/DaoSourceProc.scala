package controllers

import java.io.File

import com.google.inject.Inject
import dao.DaoReflactor
import play.api.mvc.{AbstractController, ControllerComponents}
import play.api.{Configuration, Environment}

import scala.concurrent.{ExecutionContext, Future}

class DaoSourceProc @Inject()(
                               val config: Configuration,
                               val env: Environment,
                               implicit val ec: ExecutionContext,
                               cc: ControllerComponents
                             ) extends AbstractController(cc) {

  def genDaoSource = Action.async {
    implicit req =>
      val classStr = req.getQueryString("class").getOrElse("")
      val sourceConfigPath = s"${config.get[Option[String]]("acorn.repo").getOrElse("")}${config.get[String]("acorn.daosourcepath")}"
      val sourceDir = new File(sourceConfigPath)
      if (classStr.isEmpty) {
        Future(BadRequest("Class is required!"))
      } else {
        if (sourceDir.exists()) {
          Future {
            DaoReflactor.genSources(classStr, sourceDir.getAbsolutePath)
            Ok("Generated!")
          }
        } else {
          Future(Ok("Wrong configuration!"))
        }
      }
  }
}
