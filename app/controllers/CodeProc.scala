package controllers

import business.MetaInfo
import com.google.inject.Inject
import dao.CodeDAO
import models.{Code, CodeCombo, CodeQuery, ProcStatus}
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc.{AnyContent, InjectedController, Request}
import util.StringUtil._

import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by LinY on 2017/6/20.
  */
class CodeProc @Inject()(val codeDAO: CodeDAO, val restSecured: RestSecured,
                         val configuration: Configuration, implicit val ec: ExecutionContext) extends InjectedController {

  case class CodeListEntity(meta: MetaInfo, data: Seq[Code])

  implicit val codeListEntityFormat = Json.format[CodeListEntity]

  def getCodeByCode() = restSecured.withAuthFuture {
    _: String =>
      implicit req =>
        val syscode = req.getQueryString("syscode").getOrElse("")
        val modcode = req.getQueryString("modcode").getOrElse("")
        val key = req.getQueryString("key").getOrElse("")
        codeDAO.findCode(syscode, modcode, key) map {
          case Some(c) =>
            Ok(Json.toJson(c))
          case None =>
            val status = ProcStatus()
            NotFound(Json.toJson(status))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def listCodes() = restSecured.withAuthFuture {
    _: String =>
      implicit req =>
        val syscode = req.getQueryString("syscode").getOrElse("")
        val modcode = req.getQueryString("modcode").getOrElse("")
        val key = req.getQueryString("key").getOrElse("")
        val description = req.getQueryString("description").getOrElse("")

        val query = CodeQuery(
          Code(SysCode = syscode, ModCode = modcode, Key = key, Description = description), None, None, None, None, None, None, None, None
        )

        val page = req.getQueryString("page").getOrElse("0").toIntOpt
        val limit = req.getQueryString("limit").getOrElse("0").toIntOpt

        codeDAO.listCodes(query, page, limit) map {
          case (totalpage, total, codes) =>
            Ok(Json.toJson(CodeListEntity(MetaInfo(totalpage, total), codes)))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def findCodes() = restSecured.withAuthFuture {
    _: String =>
      implicit req =>
        val syscode = req.getQueryString("syscode").getOrElse("")
        val modcode = req.getQueryString("modcode").getOrElse("")
        codeDAO.findCodes(syscode, modcode) map {
          codes =>
            Ok(Json.toJson(codes))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def findAddressCodes(sysCode: String) = restSecured.withAuthFuture {
    _: String =>
      implicit req =>
        codeDAO.findCodes(sysCode) map {
          codes =>
            Ok(Json.toJson(codes))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def findAll() = restSecured.withAuthFuture {
    _: String ⇒
      implicit req: Request[AnyContent] ⇒
        codeDAO.findAll() map {
          codes =>
            Ok(Json.toJson(codes))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def findCodeCombos(syscode: String, modcode: String) = restSecured.withAuthFuture {
    _: String =>
      implicit req =>
        codeDAO.findCodeCombos(syscode, modcode) map {
          combos =>
            val cs = Seq(CodeCombo("", "请选择")) ++ combos
            Ok(Json.toJson(cs))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def checkExistence() = restSecured.withAuthFuture {
    _: String =>
      implicit req =>
        val syscode = req.getQueryString("syscode").getOrElse("")
        val modcode = req.getQueryString("modcode").getOrElse("")
        val key = req.getQueryString("key").getOrElse("")

        codeDAO.exists(syscode, modcode, key) map {
          result =>
            Ok(Json.toJson(result))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def addCodes() = restSecured.withAuthFuture(parse.json) {
    _ ⇒
      implicit req ⇒
        req.body.validate[Seq[Code]].fold(
          (error) => {
            Future {
              val status = ProcStatus(
                Status = "1",
                EMsg = error.mkString(",")
              )
              BadRequest(Json.toJson(status))
            }
          },
          (code) => {
            codeDAO.addInBatch(code) map {
              id =>
                val status = ProcStatus(
                  RowAffected = id
                )
                Ok(Json.toJson(status))
            } recover {
              case ex: Exception ⇒
                val status = ProcStatus(
                  Status = "1",
                  EMsg = ex.getMessage
                )
                InternalServerError(Json.toJson(status))
            }
          }
        )
  }

  def addCode() = restSecured.withAuthFuture(parse.json) {
    uid ⇒
      implicit req ⇒
        req.body.validate[Code].fold(
          (error) => {
            Future {
              val status = ProcStatus(
                Status = "1",
                EMsg = error.mkString(",")
              )
              BadRequest(Json.toJson(status))
            }
          },
          (code) => {
            codeDAO.createCode(code, uid.toLong) map {
              id =>
                val status = ProcStatus(
                  Id = Some(id)
                )
                Ok(Json.toJson(status))
            } recover {
              case ex: Exception ⇒
                val status = ProcStatus(
                  Status = "1",
                  EMsg = ex.getMessage
                )
                InternalServerError(Json.toJson(status))
            }
          }
        )
  }

  def updateCode() = restSecured.withAuthFuture(parse.json) {
    uid ⇒
      implicit req ⇒
        req.body.validate[Code].fold(
          (error) => {
            Future {
              val status = ProcStatus(
                Status = "1",
                EMsg = error.mkString(",")
              )
              BadRequest(Json.toJson(status))
            }
          },
          (code) => {
            codeDAO.updateCode(code, uid.toLong) map {
              n =>
                val status = ProcStatus(
                  RowAffected = n
                )
                Ok(Json.toJson(status))
            } recover {
              case ex: Exception ⇒
                val status = ProcStatus(
                  Status = "1",
                  EMsg = ex.getMessage
                )
                InternalServerError(Json.toJson(status))
            }
          }
        )
  }

  def removeCode() = restSecured.withAuthFuture(parse.json) {
    uid ⇒
      implicit req ⇒
        req.body.validate[Code].fold(
          (error) => {
            Future {
              val status = ProcStatus(
                Status = "1",
                EMsg = error.mkString(",")
              )
              BadRequest(Json.toJson(status))
            }
          },
          (code) => {
            codeDAO.removeCode(code) map {
              n =>
                val status = ProcStatus(
                  RowAffected = n
                )
                Ok(Json.toJson(status))
            } recover {
              case ex: Exception ⇒
                val status = ProcStatus(
                  Status = "1",
                  EMsg = ex.getMessage
                )
                InternalServerError(Json.toJson(status))
            }
          }
        )
  }

  def removeCodeByModCode() = restSecured.withAuthFuture {
    uid ⇒
      implicit req ⇒
        val syscode=req.getQueryString("syscode").getOrElse("")
        val modcode=req.getQueryString("modcode").getOrElse("")
        codeDAO.removeCodeByModCode(syscode,modcode) map {
          n =>
            val status = ProcStatus(
              RowAffected = n
            )
            Ok(Json.toJson(status))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }
}
