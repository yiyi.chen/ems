package controllers

import business.{MetaInfo, UserBiz}
import com.google.inject.Inject
import dao.UserDAO
import models._
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, AnyContent, ControllerComponents, Request}
import util.StringUtil._

import scala.concurrent.{ExecutionContext, Future}

class UserProc @Inject()(
                          val userDAO: UserDAO,
                          val userBiz: UserBiz,
                          val restSecured: RestSecured,
                          val config: Configuration,
                          implicit val ec: ExecutionContext,
                          cc: ControllerComponents
                        ) extends AbstractController(cc) {

  case class UserListEntity(meta: MetaInfo, data: Seq[User])

  implicit val userListEntityFormat = Json.format[UserListEntity]

  def listUsers() = restSecured.withAuthFuture {
    uid: String ⇒
      implicit req: Request[AnyContent] ⇒
        val userame = req.getQueryString("userame").getOrElse("")
        val email = req.getQueryString("email").getOrElse("")
        val name = req.getQueryString("name").toRealNone
        val isactive = req.getQueryString("isactive").getOrElse("").toBooleanOpt
        val lastvisit = req.getQueryString("lastvisit").getOrElse("").toJodaDateTimeOpt
        val lastvisite = req.getQueryString("lastvisite").getOrElse("").toJodaDateTimeOpt

        val query = UserQuery(
          Some(User(UserName = userame, EMail = Some(email), Name = name, LastVisit = lastvisit, IsActive = isactive)),
          lastvisite
        )

        val page = req.getQueryString("page").getOrElse("0").toIntOpt
        val limit = req.getQueryString("limit").getOrElse("0").toIntOpt

        userBiz.listUsers(uid.toLong, query, page, limit) map {
          case (totalpage, total, users) =>
            Ok(Json.toJson(UserListEntity(MetaInfo(totalpage, total), users)))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def listAllUsers() = restSecured.withAuthFuture {
    _: String ⇒
      implicit req: Request[AnyContent] ⇒
        userDAO.findAll() map {
          users =>
            Ok(Json.toJson(users))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def authenticateUser = Action.async(parse.json) {
    implicit req =>
      req.body.validate[LoginEntity].fold(
        (error) => {
          Future {
            val status = ProcStatus(
              Status = "1",
              EMsg = error.mkString(",")
            )
            BadRequest(Json.toJson(status))
          }
        },
        (login) => {
          userDAO.authenticateUser(login) map {
            case (u, isActive) =>
              if (u.nonEmpty) {
                if (isActive) {
                  val status = ProcStatus(
                    Id = Some(u.get.Id)
                  )
                  Ok(Json.toJson(status)).withSession(req.session + (config.get[String]("acorn.auth.userid") -> u.get.Id.toString))
                } else {
                  Ok(Json.toJson(ProcStatus(
                    Status = "1",
                    ErrCode = 1,
                    EMsg = "User is not activated"
                  )))
                }
              } else {
                Ok(Json.toJson(ProcStatus(
                  Status = "1",
                  ErrCode = 2,
                  EMsg = "User is not found"
                )))
              }
          } recover {
            case ex: Exception ⇒
              val status = ProcStatus(
                Status = "1",
                EMsg = ex.getMessage
              )
              InternalServerError(Json.toJson(status))
          }
        }
      )
  }

  def getUserById(id: Long) = restSecured.withAuthFuture {
    _: String =>
      _: Request[AnyContent] =>
        userDAO.one(id) map {
          u =>
            Ok(Json.toJson(u))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def getUserByUserName(username: String) = restSecured.withAuthFuture {
    _: String =>
      implicit req =>
        userDAO.findByUsername(username) map {
          case Some(u) =>
            Ok(Json.toJson(u))
          case None =>
            val status = ProcStatus()
            NotFound(Json.toJson(status))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def checkUniqueUserName(id: Long, username: String) = restSecured.withAuthFuture {
    _: String =>
      implicit req =>
        userDAO.checkUniqueUsername(id, username) map {
          case true =>
            val status = ProcStatus()
            Ok(Json.toJson(status))
          case false =>
            val status = ProcStatus(
              Status = "1"
            )
            Ok(Json.toJson(status))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def checkUniqueEMail(id: Long, email: String) = restSecured.withAuthFuture {
    _: String =>
      implicit req =>
        userDAO.checkUniqueEMail(id, email) map {
          case true =>
            val status = ProcStatus()
            Ok(Json.toJson(status))
          case false =>
            val status = ProcStatus(
              Status = "1"
            )
            Ok(Json.toJson(status))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def addUser() = restSecured.withAuthFuture(parse.json) {
    uid ⇒
      implicit req ⇒
        req.body.validate[User].fold(
          (error) => {
            Future {
              val status = ProcStatus(
                Status = "1",
                EMsg = error.mkString(",")
              )
              BadRequest(Json.toJson(status))
            }
          },
          (user) => {
            user.UserName = user.UserName.toLowerCase
            userDAO.add(user, uid.toLong) map {
              id =>
                val status = ProcStatus(
                  Id = Some(id)
                )
                Ok(Json.toJson(status))
            } recover {
              case ex: Exception ⇒
                val status = ProcStatus(
                  Status = "1",
                  EMsg = ex.getMessage
                )
                InternalServerError(Json.toJson(status))
            }
          }
        )
  }

  def updateUser() = restSecured.withAuthFuture(parse.json) {
    uid ⇒
      implicit req ⇒
        req.body.validate[User].fold(
          (error) => {
            Future {
              val status = ProcStatus(
                Status = "1",
                EMsg = error.mkString(",")
              )
              BadRequest(Json.toJson(status))
            }
          },
          (user) => {
            user.UserName = user.UserName.toLowerCase
            userDAO.update(user, uid.toLong) map {
              n =>
                val status = ProcStatus(
                  RowAffected = n
                )
                Ok(Json.toJson(status))
            } recover {
              case ex: Exception ⇒
                val status = ProcStatus(
                  Status = "1",
                  EMsg = ex.getMessage
                )
                InternalServerError(Json.toJson(status))
            }
          }
        )
  }

  def changePassword(id: Long) = restSecured.withAuthFuture(parse.json) {
    uid =>
      implicit req =>
        req.body.validate[UserPassword].fold(
          (error) => {
            Future {
              val status = ProcStatus(
                Status = "1",
                EMsg = error.mkString(",")
              )
              BadRequest(Json.toJson(status))
            }
          },
          (user) => {
            userDAO.changePassword(user.Id, user.OldPassword, user.NewPassword, uid.toLong) map {
              case -1 =>
                val status = ProcStatus(
                  Status = "1",
                  EMsg = "未找到对应用户"
                )
                BadRequest(Json.toJson(status))
              case -2 =>
                val status = ProcStatus(
                  Status = "1",
                  EMsg = "用户处于非激活状态"
                )
                BadRequest(Json.toJson(status))
              case -3 =>
                val status = ProcStatus(
                  Status = "1",
                  EMsg = "原密码不正确"
                )
                BadRequest(Json.toJson(status))
              case n =>
                val status = ProcStatus(
                  RowAffected = n
                )
                Ok(Json.toJson(status))
            } recover {
              case ex: Exception ⇒
                val status = ProcStatus(
                  Status = "1",
                  EMsg = ex.getMessage
                )
                InternalServerError(Json.toJson(status))
            }
          }
        )
  }

  def activateUser(id: Long) = restSecured.withAuthFuture {
    uid: String ⇒
      implicit req ⇒
        userDAO.activateUser(id, uid.toLong) map {
          n =>
            val status = ProcStatus(
              RowAffected = n
            )
            Ok(Json.toJson(status))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def deactivateUser(id: Long) = restSecured.withAuthFuture {
    uid: String =>
      implicit req =>
        userDAO.deactivateUser(id, uid.toLong) map {
          n =>
            val status = ProcStatus(
              RowAffected = n
            )
            Ok(Json.toJson(status))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def checkUniqueNewUsername(username: String) = restSecured.withAuthFuture {
    _: String =>
      implicit req =>
        userDAO.checkUniqueNewUsername(username) map {
          case true =>
            val status = ProcStatus()
            Ok(Json.toJson(status))
          case false =>
            val status = ProcStatus(
              Status = "1"
            )
            Ok(Json.toJson(status))
        } recover {
          case ex: Exception ⇒
            val status = ProcStatus(
              Status = "1",
              EMsg = ex.getMessage
            )
            InternalServerError(Json.toJson(status))
        }
  }

  def importUsers() = restSecured.withAuthFuture(parse.json) {
    uid ⇒
      implicit req ⇒
        req.body.validate[Seq[User]].fold(
          (error) => {
            Future {
              val status = ProcStatus(
                Status = "1",
                EMsg = error.mkString(",")
              )
              BadRequest(Json.toJson(status))
            }
          },
          (users) => {
            userBiz.importUser(users, uid.toLong) map {
              id =>
                val status = ProcStatus(
                  Id = Some(id)
                )
                Ok(Json.toJson(status))
            } recover {
              case ex: Exception ⇒
                val status = ProcStatus(
                  Status = "1",
                  EMsg = ex.getMessage
                )
                InternalServerError(Json.toJson(status))
            }
          }
        )
  }
}
