package controllers

import com.google.inject.{Inject, Singleton}
import play.api.Configuration
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
final class RestSecured @Inject()(val config: Configuration, implicit val ec: ExecutionContext, cc: ControllerComponents) extends AbstractController(cc) {
  def getUserId(request: RequestHeader): Option[String] = request.session.get(config.get[String]("acorn.auth.userid"))

  def onUnauthorized(request: RequestHeader): Result = {
    Results.Unauthorized("")
  }

  def withAuth(f: => String => Request[AnyContent] => Result): EssentialAction = {
    Security.Authenticated(getUserId, onUnauthorized) {
      uid =>
        Action{
          request: Request[AnyContent] =>
            f(uid)(request)
        }
    }
  }

  def withAuthFuture(f: => String => Request[AnyContent] => Future[Result]): EssentialAction = {
    Security.Authenticated(getUserId, onUnauthorized) {
      uid =>
        Action.async(request => f(uid)(request))
    }
  }

  def withAuth[A](p: BodyParser[A])(f: => String => Request[A] => Result): EssentialAction = {
    Security.Authenticated(getUserId, onUnauthorized) {
      uid =>
        Action(p){
          request: Request[A] =>
            f(uid)(request)
        }
    }
  }

  def withAuthFuture[A](p: BodyParser[A])(f: => String => Request[A] => Future[Result]): EssentialAction = {
    Security.Authenticated(getUserId, onUnauthorized) {
      uid =>
        Action.async(p)(request => f(uid)(request))
    }
  }
}