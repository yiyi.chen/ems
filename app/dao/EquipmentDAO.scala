package dao


import com.google.inject.Inject
import models.{Equipment, EquipmentSource}
import org.joda.time.DateTime
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.ExecutionContext

trait EquipmentComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  final class EquipmentTable(tag: Tag) extends Table[Equipment](tag, "t0003") {

    import dao.CustomColumnTypes._

    def id = column[Long]("id")

    def code = column[String]("code")

    def no = column[String]("no")

    def name = column[String]("name")

    def pinyin = column[Option[String]]("pinyin")

    def modeno = column[Option[String]]("modeno")

    def manufno = column[Option[String]]("manufno")

    def checkrange = column[Option[String]]("checkrange")

    def uncertainty = column[Option[String]]("uncertainty")

    def producer = column[Option[String]]("producer")

    def usage = column[Option[String]]("usage")

    def fileno = column[Option[String]]("fileno")

    def isstandard = column[Option[Boolean]]("isstandard")

    def instruementtype = column[Option[String]]("instruementtype")

    def esprovider = column[Option[String]]("esprovider")

    def esowndept = column[Option[String]]("esowndept")

    def eskeeper = column[Option[Long]]("eskeeper")

    def escheckunit = column[Option[String]]("escheckunit")

    def essourceway = column[Option[String]]("essourceway")

    def escheckdate = column[Option[DateTime]]("escheckdate")

    def escheckdatetype = column[Option[String]]("escheckdatetype")

    def escheckinterval = column[Option[Int]]("escheckinterval")

    def escertno = column[Option[String]]("escertno")

    def esprocuprice = column[Option[BigDecimal]]("esprocuprice")

    def esprocudate = column[Option[DateTime]]("esprocudate")

    def eseffectivedate = column[Option[DateTime]]("eseffectivedate")

    def eseffectivedatetype = column[Option[String]]("eseffectivedatetype")

    def esnextcheckdate = column[Option[DateTime]]("esnextcheckdate")

    def esnextcheckdatetype = column[Option[String]]("esnextcheckdatetype")

    def essourcereq = column[Option[String]]("essourcereq")

    def esmemo = column[Option[String]]("esmemo")

    def status = column[Option[String]]("status")


    def * = (id, code, no, name, pinyin, modeno, manufno, checkrange, uncertainty, producer, usage, fileno, isstandard, instruementtype, (esprovider, esowndept, eskeeper, escheckunit, essourceway, escheckdate, escheckdatetype, escheckinterval, escertno, esprocuprice, esprocudate, eseffectivedate, eseffectivedatetype, esnextcheckdate, esnextcheckdatetype, essourcereq, esmemo), status).shaped <>
      ( {
        case (id, code, no, name, pinyin, modeno, manufno, checkrange, uncertainty, producer, usage, fileno, isstandard, instruementtype, source, status) =>
          Equipment(id, code, no, name, pinyin, modeno, manufno, checkrange, uncertainty, producer, usage, fileno, isstandard, instruementtype, Option(EquipmentSource.tupled.apply(source)), status)
      }, { e: Equipment =>
        Some((e.Id, e.Code, e.No, e.Name, e.Pinyin, e.ModeNo, e.ManufNo, e.CheckRange, e.Uncertainty, e.Producer, e.Usage, e.FileNo, e.IsStandard, e.InstruementType, EquipmentSource.unapply(e.Source.getOrElse(EquipmentSource())).get, e.Status))
      })
  }

}

class EquipmentDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                             implicit val ec: ExecutionContext) extends EquipmentComponent
  with HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  private lazy val equipments = TableQuery[EquipmentTable]

  def createTable(): Unit = {
    db.run(
      equipments.schema.create
    )
  }
}
       