package dao

import business.AcornConfig
import com.google.inject.Inject
import org.joda.time.DateTime
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import business._
import models.{AuditInfo, Code, CodeCombo, CodeQuery}

import scala.concurrent.{ExecutionContext, Future}

trait CodeComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  final class CodeTable(tag: Tag) extends Table[Code](tag, "t0002") {

    import dao.CustomColumnTypes._

    def syscode = column[String]("syscode")

    def modcode = column[String]("modcode")

    def key = column[String]("key")

    def description = column[String]("description")

    def ishardcoded = column[Option[Boolean]]("ishardcoded")

    def point01 = column[Option[Double]]("point01")

    def point02 = column[Option[Double]]("point02")

    def point03 = column[Option[Double]]("point03")

    def point04 = column[Option[Double]]("point04")

    def string01 = column[Option[String]]("string01")

    def string02 = column[Option[String]]("string02")

    def string03 = column[Option[String]]("string03")

    def string04 = column[Option[String]]("string04")

    def istag = column[Option[Boolean]]("istag")

    def date01 = column[Option[DateTime]]("date01")

    def date02 = column[Option[DateTime]]("date02")

    def date03 = column[Option[DateTime]]("date03")

    def date04 = column[Option[DateTime]]("date04")

    def ailast = column[Option[Double]]("ailast")

    def ainext = column[Option[Double]]("ainext")

    def aicreator = column[Option[Long]]("aicreator")

    def aicreatetime = column[Option[DateTime]]("aicreatetime")

    def aiupdater = column[Option[Long]]("aiupdater")

    def aiupdatetime = column[Option[DateTime]]("aiupdatetime")

    def pk = primaryKey("t0002_pk", (syscode, modcode, key))

    def * = (syscode, modcode, key, description, ishardcoded, point01, point02, point03, point04, string01, string02, string03, string04, istag, date01, date02, date03, date04, (ailast, ainext, aicreator, aicreatetime, aiupdater, aiupdatetime)).shaped <>
      ( {
        case (syscode, modcode, key, description, ishardcoded, point01, point02, point03, point04, string01, string02, string03, string04, istag, date01, date02, date03, date04, audit) =>
          Code(syscode, modcode, key, description, ishardcoded, point01, point02, point03, point04, string01, string02, string03, string04, istag, date01, date02, date03, date04, Option(AuditInfo.tupled.apply(audit)))
      }, { c: Code =>
        Some((c.SysCode, c.ModCode, c.Key, c.Description, c.IsHardCoded, c.Point01, c.Point02, c.Point03, c.Point04, c.String01, c.String02, c.String03, c.String04, c.IsTag, c.Date01, c.Date02, c.Date03, c.Date04, AuditInfo.unapply(c.Audit.getOrElse(AuditInfo())).get))
      })
  }

}

class CodeDAO @Inject()(
                         acornConfig: AcornConfig,
                         protected val dbConfigProvider: DatabaseConfigProvider,
                         implicit val ec: ExecutionContext) extends CodeComponent
  with HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  private lazy val codes = TableQuery[CodeTable]

  private def buildQuery(query: CodeQuery): Query[CodeTable, Code, Seq] = {
    import dao.CustomColumnTypes._

    codes.filter {
      c =>
        List(
          c.syscode like s"%${query.code.SysCode}%",
          c.modcode like s"%${query.code.ModCode}%",
          c.key like s"%${query.code.Key}%",
          c.description like s"%${query.code.Description}%"
        ).reduceLeftOption(_ && _).getOrElse(true: Rep[Boolean])
    }.filter {
      c =>
        List(
          query.code.IsHardCoded.map(i => c.ishardcoded === i),
          query.code.Date01.map(d => c.date01 >= d),
          query.d01e.map(d => c.date01 < d.plusDays(1)),
          query.code.Date02.map(d => c.date02 >= d),
          query.d02e.map(d => c.date02 < d.plusDays(1)),
          query.code.Date03.map(d => c.date03 >= d),
          query.d03e.map(d => c.date03 < d.plusDays(1)),
          query.code.Date04.map(d => c.date04 >= d),
          query.d04e.map(d => c.date04 < d.plusDays(1)),
          query.code.Point01.map(p => c.point01 >= p),
          query.p01e.map(p => c.point01 <= p),
          query.code.Point02.map(p => c.point02 >= p),
          query.p02e.map(p => c.point02 <= p),
          query.code.Point03.map(p => c.point03 >= p),
          query.p03e.map(p => c.point03 <= p),
          query.code.Point04.map(p => c.point04 >= p),
          query.p04e.map(p => c.point04 <= p),
          query.code.String01.map(s => c.string01 like s"%$s%"),
          query.code.String02.map(s => c.string02 like s"%$s%"),
          query.code.String03.map(s => c.string03 like s"%$s%"),
          query.code.String04.map(s => c.string04 like s"%$s%"),
          query.code.IsTag.map(i => c.istag === i)
        ).collect({ case Some(cond) => cond }).reduceLeftOption(_ && _).getOrElse(Some(true): Rep[Option[Boolean]])
    }
  }

  def createTable(): Future[Unit] = {
    db.run(codes.schema.create)
  }

  def exists(syscode: String, modcode: String, key: String): Future[Boolean] = {
    db.run(codes.filter(c => c.syscode === syscode && c.modcode === modcode && c.key === key).exists.result)
  }

  def listCodes(query: CodeQuery, page: Option[Int], limit: Option[Int]): Future[(Int, Int, Seq[Code])] = {
    val q = buildQuery(query)

    db.run(q.length.result) flatMap {
      length =>
        val paging = acornConfig.paging(length, page, limit)
        db.run(q.drop(paging.skipCount).take(paging.limit).result) map {
          codes =>
            (paging.pageCount, length, codes)
        }
    }
  }

  def findByQuery(query: CodeQuery, page: Option[Int], limit: Option[Int]): Future[(Int, Int, Seq[Code])] = {
    val q = buildQuery(query)
    db.run(q.length.result) flatMap {
      length =>
        val paging = acornConfig.paging(length, page, limit)
        db.run(
          buildQuery(query).drop(paging.skipCount).take(paging.limit).result) map {
          us =>
            (paging.pageCount, length, us)
        }
    }
  }
  def findCode(syscode: String, modcode: String, key: String): Future[Option[Code]] = {
    db.run(codes.filter(c => c.syscode === syscode && c.modcode === modcode && c.key === key).result.headOption)
  }

  def findCodeByName(syscode: String, modcode: String, name: String): Future[Option[Code]] = {
    db.run(codes.filter(c => c.syscode === syscode && c.modcode === modcode && c.description === name).result.headOption)
  }

  def findCodes(syscode: String, modcode: String): Future[Seq[Code]] = {
    db.run(codes.filter(c => c.syscode === syscode && c.modcode === modcode).result)
  }

  def findCodes(sysCode: String): Future[Seq[Code]] = {
    db.run(codes.filter(c => c.syscode === sysCode).result)
  }

  def findCodeCombos(syscode: String, modcode: String): Future[Seq[CodeCombo]] = {
    db.run(
      codes.filter(c => c.syscode === syscode && c.modcode === modcode).sortBy(_.description)
        .map(c => (c.key, c.description) <> (CodeCombo.tupled, CodeCombo.unapply)).result
    )
  }

  def findAll(): Future[Seq[Code]] = {
    db.run(codes.result)
  }

  def createCode(c: Code, uid: Long): Future[Int] = {
    c.Audit = Some(AuditInfo(
      Creator = Some(uid), CreateTime = Some(DateTime.now)
    ))
    db.run(codes += c)
  }

  def addInBatch(cs: Seq[Code]): Future[Int] = {
    db.run(
      codes ++= cs
    ) map (_ => cs.length)
  }

  def createCodes(cs: Seq[Code], uid: Long): Future[Unit] = {
    cs foreach {
      c =>
        c.Audit = Some(AuditInfo(
          Creator = Some(uid), CreateTime = Some(DateTime.now)
        ))
    }
    db.run(codes ++= cs).map(_ => ())
  }

  def updateCode(c: Code, uid: Long): Future[Int] = {
    if (c.Audit.isEmpty) {
      c.Audit = Some(AuditInfo())
    }
    c.Audit.get.Updater = Some(uid)
    c.Audit.get.UpdateTime = Some(DateTime.now)
    db.run(codes.filter(cd => cd.syscode === c.SysCode && cd.modcode === c.ModCode && cd.key === c.Key).update(c))
  }

  def removeCode(c: Code): Future[Int] = {
    db.run(codes.filter(cd => cd.syscode === c.SysCode && cd.modcode === c.ModCode && cd.key === c.Key).delete)
  }

  def removeCodeByModCode(syscode: String, modcode: String): Future[Int] = {
    db.run(codes.filter(cd => cd.syscode === syscode && cd.modcode === modcode).delete)
  }


}
       