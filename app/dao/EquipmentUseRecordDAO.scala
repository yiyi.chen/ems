package dao


import com.google.inject.Inject
import models.EquipmentUseRecord
import org.joda.time.DateTime
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.ExecutionContext

trait EquipmentUseRecordComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  final class EquipmentUseRecordTable(tag: Tag) extends Table[EquipmentUseRecord](tag, "t00031") {

    import dao.CustomColumnTypes._

    def id = column[Long]("id")

    def equipmentno = column[String]("equipmentno")

    def createtime = column[Option[DateTime]]("createtime")

    def userid = column[Long]("userid")

    def status = column[Option[String]]("status")


    def * = (id, equipmentno, createtime, userid, status).shaped <>
      ( {
        case (id, equipmentno, createtime, userid, status) =>
          EquipmentUseRecord(id, equipmentno, createtime, userid, status)
      }, { e: EquipmentUseRecord =>
        Some((e.Id, e.EquipmentNo, e.CreateTime, e.UserId, e.Status))
      })
  }

}

class EquipmentUseRecordDAO @Inject()(protected val dbConfigProvider: DatabaseConfigProvider,
                                      implicit val ec: ExecutionContext) extends EquipmentUseRecordComponent
  with HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  private lazy val equipmentuserecords = TableQuery[EquipmentUseRecordTable]

  def createTable(): Unit = {
    db.run(
      equipmentuserecords.schema.create
    )
  }
}
       