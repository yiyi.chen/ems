package dao

import java.sql.Timestamp

import org.joda.time.DateTime
import slick.jdbc.OracleProfile.api._
import slick.jdbc.SetParameter
import slick.lifted.MappedTo

object CustomColumnTypes {
  implicit val dateTimeType =
    MappedColumnType.base[DateTime, Timestamp](
      dt => new Timestamp(dt.getMillis),
      ts => new DateTime(ts.getTime)
    )

  implicit val SetDateTime = SetParameter[DateTime](
    (dt, pp) => pp.setTimestamp(new Timestamp(dt.getMillis))
  )

  final case class Id[A](value: Long) extends AnyVal with MappedTo[Long]

}
