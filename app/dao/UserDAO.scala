package dao

import business.{AcornConfig, genException}
import com.google.inject.Inject
import models.{AuditInfo, LoginEntity, User, UserQuery}
import org.joda.time.DateTime
import play.api.Logger
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile
import util.StringUtil._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{ExecutionContext, Future}

trait UserComponent {
  self: HasDatabaseConfigProvider[JdbcProfile] =>

  import profile.api._

  final class UserTable(tag: Tag) extends Table[User](tag, "t0001") {

    import dao.CustomColumnTypes._

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def keyid = column[Option[String]]("keyid")

    def deptcode = column[String]("deptcode")

    def username = column[String]("username")

    def password = column[String]("password")

    def email = column[Option[String]]("email")

    def mobile = column[Option[String]]("mobile")

    def firstname = column[Option[String]]("firstname")

    def lastname = column[Option[String]]("lastname")

    def name = column[Option[String]]("name")

    def isactive = column[Option[Boolean]]("isactive")

    def issuperuser = column[Option[Boolean]]("issuperuser")

    def lastvisit = column[Option[DateTime]]("lastvisit")

    def ailast = column[Option[Double]]("ailast")

    def ainext = column[Option[Double]]("ainext")

    def aicreator = column[Option[Long]]("aicreator")

    def aicreatetime = column[Option[DateTime]]("aicreatetime")

    def aiupdater = column[Option[Long]]("aiupdater")

    def aiupdatetime = column[Option[DateTime]]("aiupdatetime")


    def * = (id, keyid, deptcode, username, password, email, mobile, firstname, lastname, name, isactive, issuperuser, lastvisit, (ailast, ainext, aicreator, aicreatetime, aiupdater, aiupdatetime)).shaped <>
      ( {
        case (id, keyid, deptcode, username, password, email, mobile, firstname, lastname, name, isactive, issuperuser, lastvisit, audit) =>
          User(id, keyid, deptcode, username, password, email, mobile, firstname, lastname, name, isactive, issuperuser, lastvisit, Option(AuditInfo.tupled.apply(audit)))
      }, { u: User =>
        Some((u.Id, u.KeyId, u.DeptCode, u.UserName, u.Password, u.EMail, u.Mobile, u.FirstName, u.LastName, u.Name, u.IsActive, u.IsSuperuser, u.LastVisit, AuditInfo.unapply(u.Audit.getOrElse(AuditInfo())).get))
      })
  }

}

class UserDAO @Inject()(
                         acornConfig: AcornConfig,
                         protected val dbConfigProvider: DatabaseConfigProvider,
                         implicit val ec: ExecutionContext) extends UserComponent
  with HasDatabaseConfigProvider[JdbcProfile] {

  import profile.api._

  private lazy val users = TableQuery[UserTable]

  def createTable(): Unit = {
    db.run(users.schema.create)
    db.run(
      users.filter(_.username === "admin").exists.result
    ) map {
      existed =>
        if (!existed) {
          db.run(
            users += User(UserName = "admin", Password = parseToMD5("111111"), EMail = Some("a@b.com"),
              FirstName = Some("Michael"), LastName = Some("Li"), Name = Some("Michael Li"), IsActive = Some(true),
              Audit = Some(AuditInfo(Creator = Some(1), CreateTime = Some(DateTime.now))))
          )
        }
    }
  }

  def exists(username: String): Future[Boolean] = {
    db.run(
      users.filter(_.username === username).exists.result
    )
  }

  private def buildQuery(query: UserQuery): Query[UserTable, User, Seq] = {
    import dao.CustomColumnTypes._

    var q = users.filter {
      u =>
        List(
          u.username like s"%${query.User.get.UserName}%"
        ).reduceLeftOption(_ && _).getOrElse(true: Rep[Boolean])
    }.filter {
      u =>
        List(
          query.User.get.Name.map(n => u.name like s"%$n%"),
          query.User.get.IsActive.map(i => u.isactive === i),
          query.User.get.LastVisit.map(d => u.lastvisit >= d),
          query.LastVisitE.map(d => u.lastvisit < d.plusDays(1))
        ).collect({ case Some(cond) => cond }).reduceLeftOption(_ && _).getOrElse(Some(true): Rep[Option[Boolean]])
    }

    query.User foreach {
      u =>
        u.EMail foreach {
          e =>
            if (e.nonEmpty)
              q = q.filter(_.email like s"%$e%")
        }
    }
    q
  }

  def one(id: Long): Future[Option[User]] = {
    db.run(users.filter(_.id === id).result.headOption)
  }

  def findByUsername(username: String): Future[Option[User]] = {
    db.run(
      users.filter(u => u.username === username && u.isactive === true).result.headOption
    )
  }

  def findByUsernames(usernames: Seq[String]): Future[Seq[User]] = {
    db.run(
      users.filter {
        u =>
          val conds = new ArrayBuffer[Rep[Boolean]]()
          var offset = 0
          while (offset < usernames.length) {
            conds += u.username inSet usernames.slice(offset, offset + 999)
            offset += 999
          }
          if (conds.nonEmpty) {
            conds.reduceLeft(_ || _)
          } else {
            false: Rep[Boolean]
          }
      }.result
    )
  }

  def findAll(): Future[Seq[User]] = {
    db.run(users.result)
  }

  def findByQuery(query: UserQuery, page: Option[Int], limit: Option[Int]): Future[(Int, Int, Seq[User])] = {
    count(query) flatMap {
      length =>
        val paging = acornConfig.paging(length, page, limit)
        db.run(
          buildQuery(query).sortBy(_.id).drop(paging.skipCount).take(paging.limit).result
        ) map {
          us =>
            (paging.pageCount, length, us)
        }
    }
  }

  def count(): Future[Int] = {
    db.run(users.length.result)
  }

  def count(query: UserQuery): Future[Int] = {
    db.run(buildQuery(query).length.result)
  }

  def authenticateUser(lo: LoginEntity): Future[(Option[User], Boolean)] = {
    val query = users.filter(u => u.username === lo.UserName.toLowerCase && u.password === parseToMD5(lo.Password))

    db.run(
      query.result.headOption
    ) map {
      case Some(u) =>
        import dao.CustomColumnTypes._
        db.run(
          users.filter(_.id === u.Id).map(_.lastvisit).update(Some(DateTime.now))
        )
        (Some(u), u.IsActive.getOrElse(false))
      case None =>
        (None, false)
    }
  }

  def authenticateUserWithKey(lo: LoginEntity): Future[(Option[User], Boolean)] = {
    val query = users.filter(u => u.username === lo.UserName && u.password === parseToMD5(lo.Password)
      && u.keyid === lo.KeyId)
    db.run(
      query.result.headOption
    ) map {
      case Some(u) =>
        (Some(u), u.IsActive.getOrElse(false))
      case None =>
        (None, false)
    }
  }

  def checkUniqueUsername(uid: Long, username: String): Future[Boolean] = {
    val query = users.filter(u => u.isactive === true && u.username === username && u.id =!= uid)
    db.run(
      query.exists.result
    ) map {
      existed =>
        !existed
    }
  }

  def checkUniqueNewUsername(username: String): Future[Boolean] = {
    val query = users.filter(u => u.username === username)
    db.run(
      query.exists.result
    ) map {
      existed =>
        !existed
    }
  }

  def checkUniqueEMail(uid: Long, email: String): Future[Boolean] = {
    val query = users.filter(u => u.isactive === true && u.email === email && u.id =!= uid)
    db.run(
      query.exists.result
    ) map {
      existed =>
        !existed
    }
  }

  def changePassword(id: Long, oldpwd: String, newpwd: String, uid: Long): Future[Int] = {
    import dao.CustomColumnTypes._

    db.run(
      users.filter(_.id === uid).result.headOption
    ) flatMap {
      case Some(u) =>
        if (u.IsActive.getOrElse(false)) {
          if (oldpwd.nonEmpty) {
            if (u.Password != parseToMD5(oldpwd)) {
              Future(-3)
            } else {
              db.run(
                users.filter(_.id === id).map(u => (u.password, u.aiupdater, u.aiupdatetime))
                  .update((parseToMD5(newpwd), Some(uid), Some(DateTime.now)))
              ) map {
                rows =>
                  rows
              }
            }
          } else {
            db.run(
              users.filter(_.id === id).map(u => (u.password, u.aiupdater, u.aiupdatetime))
                .update((parseToMD5(newpwd), Some(uid), Some(DateTime.now)))
            ) map {
              rows =>
                rows
            }
          }
        } else {
          Future(-2)
        }
      case None =>
        Future(-1)
    }
  }

  def add(u: User, uid: Long): Future[Long] = {
    u.Audit = Some(AuditInfo(
      Creator = Some(uid), CreateTime = Some(DateTime.now)
    ))
    u.Password = parseToMD5(u.Password)
    db.run(
      users returning users.map(_.id) += u
    ) map {
      id =>
        id
    }
  }

  def addInBatch(us: Seq[User]): Future[Int] = {
    us foreach {
      u =>
        u.Password = parseToMD5(u.Password)
    }
    db.run(
      users ++= us
    ) map (_ => us.length)
  }

  def update(u: User, uid: Long): Future[Int] = {
    if (u.Audit.isEmpty) {
      u.Audit = Some(AuditInfo())
    }
    u.Audit.get.Updater = Some(uid)
    u.Audit.get.UpdateTime = Some(DateTime.now)
    db.run(
      users.filter(_.id === u.Id).update(u)
    )
  }

  def updateInbatch(us: Seq[User], uid: Long): Future[Int] = {
    us foreach {
      u =>
        if (u.Audit.isEmpty) {
          u.Audit = Some(AuditInfo())
        }
        u.Audit.get.Updater = Some(uid)
        u.Audit.get.UpdateTime = Some(DateTime.now)
        u.Password = parseToMD5(u.Password)
    }
    db.run(
      DBIO.seq(
        us.map(u => users.filter(_.id === u.Id).update(u)): _*
      ).transactionally
    ) map (_ => us.length)
  }

  def findByKey(keyid: String): Future[Option[User]] = {
    val query = users.filter(_.keyid === keyid)
    db.run(
      query.result.headOption
    )
  }

  def findByKeyNName(keyid: String, username: String): Future[Seq[User]] = {
    val query = users.filter(u => (u.username like s"%$username%") && (u.keyid like s"%$keyid%"))

    db.run(query.result)
  }

  def bindKey(userid: Long, keyid: String): Future[Int] = {
    val query = users.filter(_.id === userid) map {
      u =>
        u.keyid
    }
    db.run(
      query.update(Some(keyid))
    )
  }

  def unbindKey(userid: Long): Future[Int] = {
    val query = users.filter(_.id === userid) map {
      u =>
        u.keyid
    }
    db.run(
      query.update(None)
    )
  }

  def deactivateUser(uid: Long, updaterId: Long): Future[Int] = {
    import dao.CustomColumnTypes._

    db.run(
      users.filter(_.id === uid)
        .map(u => (u.isactive, u.aiupdater, u.aiupdatetime))
        .update((Some(false), Some(updaterId), Some(DateTime.now)))
    )
  }

  def activateUser(uid: Long, updaterId: Long): Future[Int] = {
    import dao.CustomColumnTypes._

    db.run(
      users.filter(_.id === uid)
        .map(u => (u.isactive, u.aiupdater, u.aiupdatetime))
        .update((Some(true), Some(updaterId), Some(DateTime.now)))
    )
  }

  def checkIsSupUser(uid: Long): Future[Boolean] = {
    db.run(
      users.filter(_.id === uid).result.headOption
    ) map (
      r =>
        if (r.isEmpty) {
          false
        } else {
          r.get.IsSuperuser.getOrElse(false)
        }
      )
  }
}
       