name := "ems"
 
version := "1.0" 
      
lazy val `ems` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
      
scalaVersion := "2.12.8"

libraryDependencies ++= Seq( jdbc , ehcache , ws , specs2 % Test , guice,
  "com.typesafe.play" %% "play-json" % "2.7.2",
  "com.typesafe.play" %% "play-slick" % "3.0.0",
  "com.typesafe.slick" %% "slick" % "3.3.0",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.0",
  "org.postgresql" % "postgresql" % "42.2.5",
  "org.scala-lang" % "scala-reflect" % "2.12.8",
  "org.apache.poi" % "poi" % "4.0.1",
  "com.belerweb" % "pinyin4j" % "2.5.1"
)

      